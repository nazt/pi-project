import sys
import threading
import time
import random
import string
from Pubnub import Pubnub
from piface import pfio

pfio.init()
relay = [pfio.Relay(0), pfio.Relay(1)]

## Initiate Class
pubnub = Pubnub( 'pub-c-0a5aa9d7-02cc-4f29-9c97-4d3601d30656', 'sub-c-b47bf35e-a98b-11e2-821b-12313f022c90', None, False )

print("My UUID is: "+pubnub.uuid)

channel = 'pfio'

## Subscribe Example
def receive(message) :
    print(message)

    rid = int(message['body'])-1
    _relay = relay[rid]
    _relay.toggle()

    ## Publish Example
    try:
        info = pubnub.publish({
            'channel' : 'pfio_present',
            'message' : {
                'id': int(rid),
                'status': _relay.value,
            }
        })
    except Exception:
        pass

    return False 

while True:
    print ("subscribed")
    pubnub.subscribe({
        'channel'  : channel,
        'callback' : receive 
    }) 

