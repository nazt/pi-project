import time
import serial
import requests
import sys
from datetime import datetime

# configure the serial connections (the parameters differs on the device you are connecting to) 
s = serial.Serial(
    port='/dev/ttyUSB0',
    baudrate=9600,
    parity=serial.PARITY_ODD,
    stopbits=serial.STOPBITS_TWO,
    bytesize=serial.SEVENBITS
)

def make_request_string(cmd_list):
    node = int(cmd_list[0], 2)
    out_str = ""
    if len(cmd_list) >= 3:
        out_str ="node=%d&temperature=%s&humidity=%s" % (node, cmd_list[1], cmd_list[2])
    else:
        out_str ="node=%d&temperature=%s" % (node, cmd_list[1])
        
    out_str += "&time=%s" % (ctime)
    return out_str


def make_post_obj(cmd_list):
    node = int(cmd_list[0], 2)
    temperature = cmd_list[1]
    post_obj = { }
    if len(cmd_list) >= 3:
        post_obj = dict(node=node, temperature=temperature, humidity=cmd_list[2], machine='wireless-temperature')
    else:
        post_obj = dict(node=node, temperature=temperature, humidity='-', machine='wireless-temperature-2nd')

    return post_obj

cmd = ""
cmdList = []
data = { }
while True:
    try:
        ctime = datetime.now().strftime('%H:%M:%S')
        cmd = s.readline().strip().decode("utf-8")
        print("cmdja", cmd)
        post_obj = make_post_obj(cmd.split(','))
        post_obj['time'] = ctime
        data[post_obj['machine']] = post_obj

        # break if collected all data
        if len(data) > 1:
            break
    except requests.ConnectionError as e:
        print ("CONNECTION ERROR")
    except Exception:
        print("Unexpected error:", sys.exc_info()[0])
        pass

print (data)
#r = requests.post("http://flexi-sheet.herokuapp.com/debug", data=post_obj)

for key in data:
    r = requests.post("http://flexi-sheet.herokuapp.com/debug", data=data[key])
    print (r.text)
