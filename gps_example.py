import gps
import subprocess

# Listen on port 2947 (gpsd) of localhost
session = gps.gps("localhost", "2947")
session.stream(gps.WATCH_ENABLE | gps.WATCH_NEWSTYLE)

while True:
  try:
    report = session.next()
    # Wait for a 'TPV' report and display the current time
    # To see all report data, uncomment the line below
    # print report
    #for i in session.satellites:
    #    print '\t', i

    if report['class'] == 'TPV':
      if hasattr(report, 'lat'):
        print report.lat, report.lon
        filepath = subprocess.check_output('/home/pi/camera-module/take-a-picture.sh', shell=True).split('\n')[0]
        print filepath 
        gps_string = '/home/pi/set_gps.py %s %s %s' % (filepath, report.lat, report.lon)
        print gps_string
        output = subprocess.check_call(gps_string, shell=True)
        print(output)

      if hasattr(report, 'time'):
        pass
        print report.time

  except KeyError:
    pass
  except KeyboardInterrupt:
    quit()
  except StopIteration:
    session = None
    print "GPSD has terminated"

