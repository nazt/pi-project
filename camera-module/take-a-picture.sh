#!/bin/sh
#current_time=$(date "+%Y.%m.%d-%H.%M.%S")
#echo $current_time;

#ROLL=$(cat /var/tlcam/series)
ROLL='cube-'


#SAVEDIR=/home/pi/camera-module
SAVEDIR=/home/pi/camera-module/pictures

while [ true ]; do

filename=$ROLL-$(date +"%d%m%Y_%H%M-%S").jpg
echo "Taking... $filename"

#/opt/vc/bin/raspistill -hf -o $SAVEDIR/$filename
/opt/vc/bin/raspistill -hf -q 80 -w 1024 -h 768 -o $SAVEDIR/$filename -t 2

echo "Done..."
echo "$SAVEDIR/$filename"
sleep 4;

done;
