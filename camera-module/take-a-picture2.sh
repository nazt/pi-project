#current_time=$(date "+%Y.%m.%d-%H.%M.%S")
#echo $current_time;

#ROLL=$(cat /var/tlcam/series)
ROLL='test-'


SAVEDIR=/home/pi/camera-module
#SAVEDIR=/media1/camera-module

while [ true ]; do

filename=$ROLL-$(date -u +"%d%m%Y_%H%M-%S").jpg

/opt/vc/bin/raspistill -w 300 -h 300 -q 100 -th -o $SAVEDIR/$filename

sleep 4;

done;
